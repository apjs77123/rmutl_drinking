<?php
header("Access-Control-Allow-Origin: *");
header('Access-Control-Allow-Headers: Origin, Content-Type, X-Auth-Token');
header("Access-Control-Allow-Methods: Select,POST");
header("Access-Control-Allow-Credentials: true");
header('Content-Type: application/json;charset=utf-8');
date_default_timezone_set('Asia/Bangkok');

use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Message\ResponseInterface;
require_once 'vendor/autoload.php';

$app = new \Slim\App;

require_once 'config/config.php';
require_once 'app/service/SignIn.php';
require_once 'app/service/SignInEmployee.php';
require_once 'app/service/SignUp.php';
require_once 'app/service/UpdateEmail.php';
require_once 'app/service/UpdatePassword.php';
require_once 'app/home/SelectDataUser.php';
require_once 'app/user/SelectDataUserById.php';
require_once 'app/home/SelectProduct.php';

//manageorder
require_once 'app/manageorder/SelectOrderUser.php';
require_once 'app/manageorder/SelectAllOrder.php';
require_once 'app/manageorder/SelectOrderDetail.php';
require_once 'app/manageorder/InsertOrder.php';
require_once 'app/manageorder/InsertOrderDetail.php';
require_once 'app/manageorder/UpdatePayment.php';
require_once 'app/manageorder/DeleteOrder.php';
require_once 'app/manageorder/UpdateStatus.php';
require_once 'app/manageorder/DeletePaymentPicture.php';
// //uploadimage
require_once 'app/upload/UploadUserProfile.php';
require_once 'app/upload/UploadPayment.php';

// //update

require_once 'app/user/UpdateUser.php';
require_once 'app/user/UpdateProfilePicture.php';

// //delete
require_once 'app/user/DeleteProfilePicture.php';

// //manageemployee
require_once 'app/manageemployee/SelectEmployee.php';
require_once 'app/manageemployee/UpdateEmployee.php';
require_once 'app/manageemployee/DeleteEmployee.php';
require_once 'app/manageemployee/AddEmployee.php';
require_once 'app/manageemployee/UploadEmployeePicture.php';
require_once 'app/manageemployee/UpdateEmployeePicture.php';
require_once 'app/manageemployee/DeleteEmployeePicture.php';
// //manageproduct
require_once 'app/manageproduct/AddNewProduct.php';
require_once 'app/manageproduct/DeleteProductPicture.php';
require_once 'app/manageproduct/UpdateDataProduct.php';
require_once 'app/manageproduct/UpdateProductPicture.php';
require_once 'app/manageproduct/UploadProductPicture.php';
require_once 'app/manageproduct/DeleteProduct.php';

// //managestaple
require_once 'app/managestaple/AddStaple.php';
require_once 'app/managestaple/UpdateStaple.php';
require_once 'app/managestaple/DeleteStaple.php';
require_once 'app/managestaple/UpdateStaplePicture.php';
require_once 'app/managestaple/UploadStaplePic.php';
require_once 'app/managestaple/SelectStaple.php';
require_once 'app/managestaple/AddAmountStaple.php';
// //managepackaging
require_once 'app/managepackaging/AddPackaging.php';
require_once 'app/managepackaging/UpdatePackaging.php';
require_once 'app/managepackaging/DeletePackaging.php';
require_once 'app/managepackaging/UpdatePackagingPicture.php';
require_once 'app/managepackaging/UploadPackagingPic.php';
require_once 'app/managepackaging/SelectPackaging.php';
require_once 'app/managepackaging/AddAmountPackaging.php';
// //withdrawstaple
require_once 'app/withdrawstaple/insertstaplelist.php';
require_once 'app/withdrawstaple/insertstapledetail.php';
require_once 'app/withdrawstaple/Selectstapledetail.php';
require_once 'app/withdrawstaple/Selectstaplelist.php';
require_once 'app/withdrawstaple/Updatestplestatus.php';
require_once 'app/withdrawstaple/UpdateAmountStaple.php';
require_once 'app/withdrawstaple/DeleteWtihdrawStapleDetail.php';
require_once 'app/withdrawstaple/DeleteWithdrawStaple.php';
// //withdrawproduct 
require_once 'app/withdrawproduct/insertproductlist.php';
require_once 'app/withdrawproduct/insertproductdetail.php';
require_once 'app/withdrawproduct/selectproductlist.php';
require_once 'app/withdrawproduct/selectproductdetail.php';
require_once 'app/withdrawproduct/Updateproductstatus.php';
require_once 'app/withdrawproduct/UpdateAmountProduct.php';

//withdrawpackaging
require_once 'app/withdrawpackaging/InsertPackagingDetail.php';
require_once 'app/withdrawpackaging/InsertPackagingList.php';
require_once 'app/withdrawpackaging/SelectPackagingList.php';
require_once 'app/withdrawpackaging/SelectPackagingDetail.php';
require_once 'app/withdrawpackaging/UpdatePackagingStatus.php';
require_once 'app/withdrawpackaging/DeleteWithdrawDetail.php';
require_once 'app/withdrawpackaging/UpdateWithdrawPackaging.php';
require_once 'app/withdrawpackaging/UpdateAmountPackaging.php';
require_once 'app/withdrawpackaging/DeleteWithdrawPackaging.php';
//manangeshipping
require_once 'app/manageordertosend/SelectOrderToSend.php';
require_once 'app/manageordertosend/SelectShippingList.php';
require_once 'app/manageordertosend/SelectShippingDetail.php';
require_once 'app/manageordertosend/InsertShippingList.php';
require_once 'app/manageordertosend/InsertShippingDetail.php';
require_once 'app/manageordertosend/UpdateShippingDate.php';
require_once 'app/manageordertosend/DeleteShipping.php';
//manageproducttion
require_once 'app/manageproducttion/InsertProducttionList.php';
require_once 'app/manageproducttion/InsertProducttionDetail.php';
require_once 'app/manageproducttion/SelectProducttionList.php';
require_once 'app/manageproducttion/SelectProducttionDetail.php';
require_once 'app/manageproducttion/UpdateProtucttionStatus.php';
require_once 'app/manageproducttion/UpdateProducttionNamePickup.php';
require_once 'app/manageproducttion/DeleteProducttionDetail.php';
require_once 'app/manageproducttion/DeleteProducttion.php';
//transport
require_once 'app/transport/SelectDailyOrder.php';
require_once 'app/transport/SelectDailyShipping.php';
require_once 'app/transport/UpdateShipping.php';
require_once 'app/transport/UpdateShippingStatus.php';

//cost
require_once 'app/cost/SelectCostMonth.php';
require_once 'app/cost/InsertCost.php';
require_once 'app/cost/DeleteCost.php';

//report
require_once 'app/report/ReportCost.php';
require_once 'app/report/ReportProducttion.php';
require_once 'app/report/ReportSale.php';
require_once 'app/report/ReportWithdrawProduct.php';
require_once 'app/report/ReportWithdrawStaple.php';
require_once 'app/report/ReportWithdrawPackaging.php';
require_once 'app/report/ReportAddStaple.php';
require_once 'app/report/ReportAddPackaging.php';
require_once 'app/report/ReportGetPackaging.php';
$app->run();

?>
