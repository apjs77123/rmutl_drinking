<?php
  $app->post( '/insertshippingdetail',function($request,$response){
    $con = connect();
    $decodeJson = (json_decode(file_get_contents("php://input"), true));
 
    $insert = "INSERT INTO shippingdetail(spid,oid) 
    VALUES (
        {$decodeJson['id']},
        {$decodeJson['orderid']}
    )";

    $resultproduct = $con->query($insert);
  
    if($resultproduct === true){ 
        $myObj = new \stdClass();
        $myObj->status = true;
        $myObj->data = 'success';
        $myObj->obj = $decodeJson;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }else{
        return 'unsuccess';
    }

   
}); 