<?php
$app->post( '/selectshippinglist',function($request,$response){
    $con = connect();
    $decodeJson = (json_decode(file_get_contents("php://input"), true));
  
    $sqlGetOrderByuser = "SELECT * FROM shippinglist  ORDER BY spid DESC";
    $result = $con->query($sqlGetOrderByuser);
    $arrObj = array();
    foreach($result as $value){
        array_push($arrObj,$value);
    }
  
    if($arrObj != []){
        $myObj = new \stdClass();
        $myObj->status = true;
        $myObj->data = 'success.';
        $myObj->obj = $arrObj;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }else{
        $myObj = new \stdClass();
        $myObj->status = false;
        $myObj->data = 'no success';
        $myObj->obj = '';
        $myJSON = json_encode($myObj);
        return $myJSON;
    }
}); 