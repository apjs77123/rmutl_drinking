<?php
$app->post('/uploadproductpicture', function ($request, $response) {
    $fileToUpload=$_FILES["fileToUpload"];
    $target_dir = "./image/product/";
    $dtime=str_shuffle(date("123456789"."dmYhis"));
    $target_file = $target_dir . basename($dtime.$fileToUpload["name"]);
    $uploadOk = 1;
    $filePathResponse = strtolower($target_file);
    $imageFileType = strtolower(pathinfo($target_file,PATHINFO_EXTENSION));
    move_uploaded_file($fileToUpload["tmp_name"], $target_file);
    $myObj = new \stdClass();
    $myObj->status = true;
    $myObj->data = 'upload files success.';
    $myObj->obj = basename($dtime.$fileToUpload["name"]);
    $myJSON = json_encode($myObj);
    return $myJSON;
});
?>