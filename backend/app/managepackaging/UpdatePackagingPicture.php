<?php
 $app->post('/updatepackagingpicture', function ($request, $response) {

    $decodeJson = (json_decode(file_get_contents("php://input"), true));
    $con = connect();
    $oldpicture= $decodeJson['oldpicture'];
    $picture= $decodeJson['picture'];
    $id= $decodeJson['id'];
    $sl="SELECT * FROM packaging WHERE pkpicture='$oldpicture'";
    $dt= $con->query($sl);
    if($dt->num_rows !== 0){
        unlink("image/packaging/$oldpicture");
        $sql = "UPDATE packaging 
                SET 
                pkpicture='$picture'
                WHERE pkid = $id";
        $r = $con->query($sql);
        if($r === true){
            $myObj = new \stdClass();
            $myObj->status = true;
            $myObj->data = 'Update Packaging picture success.';
            $myObj->obj = null;
            $myJSON = json_encode($myObj);
            return $myJSON;
        }else{
            $myObj = new \stdClass();
            $myObj->status = false;
            $myObj->data = 'Update  Packaging picture  failed.';
            $myObj->obj = null;
            $myJSON = json_encode($myObj);
            return $myJSON;
        }
    }else{
        $myObj = new \stdClass();
        $myObj->status = true;
        $myObj->data = 'delete packagingpic unsuccess';
        $myJSON = json_encode($myObj);
        return $myJSON;
    }

});