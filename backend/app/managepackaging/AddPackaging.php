<?php
  $app->post( '/addpackaging',function($request,$response){
    $con = connect();
    $decodeJson = (json_decode(file_get_contents("php://input"), true));
    $insert = "INSERT INTO packaging (pkname,pkdetail,pkamount,pkpicture) 
    VALUES (
    '{$decodeJson['name']}',
    '{$decodeJson['detail']}',
    '{$decodeJson['amount']}',
    '{$decodeJson['picture']}'
   
    )";

    $resultproduct = $con->query($insert);
  
    if($resultproduct === true){ 
        $myObj = new \stdClass();
        $myObj->status = true;
        $myObj->data = 'packaging Success';
        $myObj->obj = $decodeJson;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }else{
        return 'เพิ่มวัตถุดิบไม่สำเร็จ';
    }

   
}); 