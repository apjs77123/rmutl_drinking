<?php
  $app->post( '/addorderdetail',function($request,$response){
    $con = connect();
    $decodeJson = (json_decode(file_get_contents("php://input"), true));
    $insert = "INSERT INTO orderdetail (oid,pid,amount) 
    VALUES (
        {$decodeJson['od_id']},
        {$decodeJson['pid']},
        {$decodeJson['pamount']}
      
    )";
    $insert = $con->query($insert)or die ("SQL ERROR1".mysqli_error($con));
    if($insert === true){ 
        $myObj = new \stdClass();
        $myObj->status = true;
        $myObj->data = 'can insert orderdetail';
        $myObj->obj = $decodeJson;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }else{
        $myObj = new \stdClass();
        $myObj->status = false;
        $myObj->data = 'cant insert orderdetail';
        $myObj->obj = $decodeJson;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }
}); 