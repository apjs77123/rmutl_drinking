<?php
 $app->post('/deletepaymentpicture', function ($request, $response) {

    $decodeJson = (json_decode(file_get_contents("php://input"), true));
    $con = connect();
    $payment= $decodeJson['payment'];
    $sl="SELECT * FROM orderlist WHERE payment='$payment'";
    $dt= $con->query($sl);
    if($dt->num_rows === 0){
        $myObj = new \stdClass();
        $myObj->status = false;
        $myObj->data = 'ลบรูปภาพไม่สำเร็จ.';
        $myObj->obj = null;
        $myJSON = json_encode($myObj);
    }else{
        unlink("image/payment/$payment");
        $myObj = new \stdClass();
        $myObj->status = true;
        $myObj->data = 'deletesuccess.';
        $myJSON = json_encode($myObj);
    }
    return $myJSON; 


});