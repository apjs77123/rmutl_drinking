<?php

  $app->post( '/addorder',function($request,$response){
    $con = connect();
    $decodeJson = (json_decode(file_get_contents("php://input"), true));
    $date=date("Y-m-d H:i:s");
    $insert = "INSERT INTO orderlist (uid,type,user,phone,address,subdistrict,area,date,total,howpay,status) 
    VALUES (
        {$decodeJson['cid']},
        '{$decodeJson['type']}',
        '{$decodeJson['email']}',
        '{$decodeJson['od_phone']}',
        '{$decodeJson['od_address']}',
        '{$decodeJson['od_subdistrict']}',
        '{$decodeJson['od_area']}',
        '$date',
         {$decodeJson['total']},
        '{$decodeJson['howpay']}',
        '{$decodeJson['status']}'
    )";
    $insert = $con->query($insert)or die ("SQL ERROR1".mysqli_error($con));
    $select="SELECT MAX(oid) FROM orderlist ";
  
    $result = $con->query($select);
    list($max_oid)=mysqli_fetch_row($result);
    if($insert === true){ 
        $myObj = new \stdClass();
        $myObj->status = true;
        $myObj->data = 'can insert order';
        $myObj->obj = $max_oid;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }else{
        $myObj = new \stdClass();
        $myObj->status = false;
        $myObj->data = 'cant insert order';
        $myObj->obj = $decodeJson;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }

   
}); 