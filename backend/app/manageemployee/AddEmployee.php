<?php

  $app->post( '/addemployee',function($request,$response){
    $con = connect();
    $decodeJson = (json_decode(file_get_contents("php://input"), true));
    $insert = "INSERT INTO employee (profilepic,userid,password,firstname,lastname,address,subdistrict,district,province,postcode,phone,department,userlevel) 
    VALUES (
        
         '{$decodeJson['profilepic']}',
        '{$decodeJson['userid']}',
        '{$decodeJson['password']}',
        '{$decodeJson['firstname']}',
        '{$decodeJson['lastname']}',
        '{$decodeJson['address']}',
        '{$decodeJson['subdistrict']}',
        '{$decodeJson['district']}',
        '{$decodeJson['province']}',
        '{$decodeJson['postcode']}',
        '{$decodeJson['phone']}',
        '{$decodeJson['department']}',
        '{$decodeJson['userlevel']}'

        
    )";
    $insert = $con->query($insert);
    if($insert === true){ 
        $myObj = new \stdClass();
        $myObj->status = true;
        $myObj->data = ' insert employee succed';
        $myObj->obj = $decodeJson;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }else{
        $myObj = new \stdClass();
        $myObj->status = false;
        $myObj->data = ' insert employee fail';
        $myObj->obj = $decodeJson;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }

   
});  
