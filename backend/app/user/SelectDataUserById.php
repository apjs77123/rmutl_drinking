<?php
$app->post( '/selectdatauserbyid',function($request,$response){
    $con = connect();
    $decodeJson = (json_decode(file_get_contents("php://input"), true));
    $id= $decodeJson['id'];
    $sl="SELECT * FROM user WHERE uid=$id";
    $dt= $con->query($sl);
    if($dt->num_rows === 0){
        $myObj = new \stdClass();
        $myObj->success = false;
        $myObj->data = 'เรียกข้อมูลไม่ได้.';
        $myObj->obj = null;
        $myJSON = json_encode($myObj);
    }else{
        $myObj = new \stdClass();
        $obj = [];
        foreach($dt as $value){
            $obj=$value;
        }
        $myObj->status = true;
        $myObj->data = 'เรียกข้อมูลสำเร็จ.';
        $myObj->obj = $obj;
        $myJSON = json_encode($myObj);
    }
    return $myJSON; 
}); 

