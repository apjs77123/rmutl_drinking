<?php

  $app->post( '/insertcost',function($request,$response){
    $con = connect();
    $decodeJson = (json_decode(file_get_contents("php://input"), true));
    $insert = "INSERT INTO cost (eid,costlist,costprice) 
    VALUES (
        {$decodeJson['eid']},
        '{$decodeJson['costlist']}',
        '{$decodeJson['costprice']}'
    )";
    $insert = $con->query($insert);
    if($insert === true){ 
        $myObj = new \stdClass();
        $myObj->status = true;
        $myObj->data = ' insert cost succed';
        $myObj->obj = $decodeJson;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }else{
        $myObj = new \stdClass();
        $myObj->status = false;
        $myObj->data = ' insert cost fail';
        $myObj->obj = $decodeJson;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }

   
});  
