<?php

  $app->post( '/addstaplelist',function($request,$response){
    $con = connect();
    $decodeJson = (json_decode(file_get_contents("php://input"), true));
    $withdrawdate=date("Y-m-d H:i:s");
    $insert = "INSERT INTO withdrawstaplelist (eid,namepicker,withdrawdate,status) 
    VALUES (
         {$decodeJson['eid']},
        '{$decodeJson['namepicker']}',
        '{$withdrawdate}',
        '{$decodeJson['status']}'
      
    )";
    $insert = $con->query($insert);
    $select="SELECT MAX(wsid) FROM withdrawstaplelist ";
  
    $result = $con->query($select);
    list($max_oid)=mysqli_fetch_row($result);
    if($insert === true){ 
        $myObj = new \stdClass();
        $myObj->status = true;
        $myObj->data = 'can insert order';
        $myObj->obj = $max_oid;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }else{
        $myObj = new \stdClass();
        $myObj->status = false;
        $myObj->data = 'cant insert order';
        $myObj->obj = $decodeJson;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }

   
}); 