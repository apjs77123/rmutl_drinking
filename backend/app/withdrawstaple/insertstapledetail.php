<?php
  $app->post( '/addstapledetail',function($request,$response){
    $con = connect();
    $decodeJson = (json_decode(file_get_contents("php://input"), true));
    $insert = "INSERT INTO withdrawstapledetail (wsid,sid,amount) 
    VALUES (
         {$decodeJson['id']},
        '{$decodeJson['sid']}',
        '{$decodeJson['samount']}'
    )";
    $insert = $con->query($insert);
    if($insert === true){ 
        $myObj = new \stdClass();
        $myObj->status = true;
        $myObj->data = 'insert staple orderdetail success';
        $myObj->obj = $decodeJson;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }else{
        $myObj = new \stdClass();
        $myObj->status = false;
        $myObj->data = 'insert orderdetail failed';
        $myObj->obj = $decodeJson;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }
}); 