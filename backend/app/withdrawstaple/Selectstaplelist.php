<?php
$app->post( '/selectallstaplelist',function($request,$response){
    $con = connect();
    $decodeJson = (json_decode(file_get_contents("php://input"), true));
  
    $select = "SELECT * FROM withdrawstaplelist ORDER BY wsid DESC";
    $result = $con->query($select);
    $arrObj = array();
    foreach($result as $value){
        array_push($arrObj,$value);
    }
  
    if($arrObj != []){
        $myObj = new \stdClass();
        $myObj->status = true;
        $myObj->data = 'success.';
        $myObj->obj = $arrObj;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }else{
        $myObj = new \stdClass();
        $myObj->status = false;
        $myObj->data = 'no success';
        $myObj->obj = '';
        $myJSON = json_encode($myObj);
        return $myJSON;
    }
}); 