<?php

$app->post( '/selectstapledetail',function($request,$response){
    $con = connect();
    $decodeJson = (json_decode(file_get_contents("php://input"), true));
    $id = $decodeJson['id'];
    $select = "SELECT * FROM withdrawstapledetail   INNER JOIN staple ON withdrawstapledetail.sid=staple.sid WHERE wsid='$id'  ";
    $result = $con->query($select);
    $arrObj = array();
    foreach($result as $value){
        array_push($arrObj,$value);
    }
  
    if($arrObj != []){
        $myObj = new \stdClass();
        $myObj->status = true;
        $myObj->data = 'a.';
        $myObj->obj = $arrObj;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }else{
        $myObj = new \stdClass();
        $myObj->status = false;
        $myObj->data = 'b';
        $myObj->obj = '';
        $myJSON = json_encode($myObj);
        return $myJSON;
    }
}); 