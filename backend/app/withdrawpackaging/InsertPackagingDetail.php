<?php
  $app->post( '/insertpackagindetail',function($request,$response){
    $con = connect();
    $decodeJson = (json_decode(file_get_contents("php://input"), true));
    $insert = "INSERT INTO withdrawpackagingdetail (wpkid,pkid,amount) 
    VALUES (
         {$decodeJson['id']},
        '{$decodeJson['pkid']}',
        '{$decodeJson['pkamount']}'
    )";
    $insert = $con->query($insert);
    if($insert === true){ 
        $myObj = new \stdClass();
        $myObj->status = true;
        $myObj->data = 'insert packaging  success';
        $myObj->obj = $decodeJson;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }else{
        $myObj = new \stdClass();
        $myObj->status = false;
        $myObj->data = 'insert  failed';
        $myObj->obj = $decodeJson;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }
}); 