<?php
  $app->post( '/insertproducttionlist',function($request,$response){
    $con = connect();
    $decodeJson = (json_decode(file_get_contents("php://input"), true));
    $insert = "INSERT INTO producttionlist (eid,time,nameproducttion,date,status) 
    VALUES (
        {$decodeJson['eid']},
        '{$decodeJson['time']}',
        '{$decodeJson['nameproducttion']}',
        '{$decodeJson['date']}',
        '{$decodeJson['status']}'
    )";
    $insert = $con->query($insert)or die ("SQL ERROR1".mysqli_error($con));
    $select="SELECT MAX(pdid) FROM producttionlist ";
    $result = $con->query($select);
    list($max_oid)=mysqli_fetch_row($result);
    if($insert === true){ 
        $myObj = new \stdClass();
        $myObj->status = true;
        $myObj->data = 'can insert list';
        $myObj->obj = $max_oid;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }else{
        $myObj = new \stdClass();
        $myObj->status = false;
        $myObj->data = 'cant insert list';
        $myObj->obj = $decodeJson;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }

   
}); 