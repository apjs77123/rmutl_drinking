<?php
  $app->post( '/insertproducttiondetail',function($request,$response){
    $con = connect();
    $decodeJson = (json_decode(file_get_contents("php://input"), true));
    $insert = "INSERT INTO producttiondetail (pdid,pid,amount) 
    VALUES (
         {$decodeJson['id']},
        '{$decodeJson['pid']}',
     
         {$decodeJson['pamount']}
    )";
    $insert = $con->query($insert);
    if($insert === true){ 
        $myObj = new \stdClass();
        $myObj->status = true;
        $myObj->data = 'can insert order';
        $myObj->obj = $decodeJson;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }else{
        $myObj = new \stdClass();
        $myObj->status = false;
        $myObj->data = 'cant insert order';
        $myObj->obj = $decodeJson;
        $myJSON = json_encode($myObj);
        return $myJSON;
    }

   
}); 