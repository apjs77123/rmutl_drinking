import Vue from 'vue';
import Router from 'vue-router';

import Signup from "@/view/mainpage/Signup";
import Signin from "@/view/mainpage/Signin";
import Home from '@/view/mainpage/Home.vue'
import Cart from '@/view/mainpage/Cart.vue'
import OrderConfirm from "@/view/mainpage/OrderConfirm";
import EditProfile from "@/view/mainpage/EditProfile";
import EditUserPassword from "@/view/mainpage/EditUserPassword";

import SigninEmployee from "@/view/mainpage/SigninEmployee";

import ManageProduct from "@/view/manager/manageproduct/ManageProduct";
import AddProduct from "@/view/manager/manageproduct/AddProduct";
import UpdateProduct from "@/view/manager/manageproduct/UpdateProduct";

import ManageStaple from "@/view/manager/managestaple/ManageStaple";
import AddStaple from "@/view/manager/managestaple/AddStaple";
import UpdateStaple from "@/view/manager/managestaple/UpdateStaple";

import ManagePackaging from "@/view/manager/managepackaging/ManagePackaging";
import AddPackaging from "@/view/manager/managepackaging/AddPackaging";
import UpdatePackaging from "@/view/manager/managepackaging/UpdatePackaging";

import ManageEmployee from "@/view/manager/manageemployee/ManageEmployee";
import AddEmployee from "@/view/manager/manageemployee/AddEmployee";
import UpdateEmployee from "@/view/manager/manageemployee/UpdateEmployee";

import ManageOrder from "@/view/manager/manageorder/ManageOrder";
import AddNewOrder from "@/view/manager/manageorder/AddNewOrder";


import ManageWithdrawStaple from "@/view/manager/managewithdrawstaple/ManageWithdrawStaple";
// import AddWithdrawStaple from "@/view/manager/managewithdrawstaple/AddWithdrawStaple";
import UpdateWithdrawStaple from "@/view/manager/managewithdrawstaple/UpdateWithdrawStaple";

import ManageWithdrawProduct from "@/view/manager/managewithdrawproduct/ManageWithdrawProduct";
// import AddWithdrawProduct from "@/view/manager/managewithdrawproduct/AddWithdrawProduct";

import ManageWithdrawPackaging from "@/view/manager/managewithdrawpackaging/ManageWithdrawPackaging";
// import AddWithdrawPackaging from "@/view/manager/managewithdrawpackaging/AddWithdrawPackaging";
import UpdateWithdrawPackaging from "@/view/manager/managewithdrawpackaging/UpdateWithdrawPackaging";

import ManageShipping from "@/view/manager/manageshipping/ManageShipping";
import AddNewShipping from "@/view/manager/manageshipping/AddNewShipping";

import ManageProducttion from "@/view/manager/manageproducttion/ManageProducttion";
// import AddNewProducttion from "@/view/manager/manageproducttion/AddNewProducttion";
import UpdateProducttion from "@/view/manager/manageproducttion/UpdateProducttion";

import Cost from "@/view/manager/managecost/Cost";

import ReportCost from "@/view/manager/report/ReportCost";
import ReportProducttion from "@/view/manager/report/ReportProducttion";
import ReportSale from "@/view/manager/report/ReportSale";
import ReportStockPackaging from "@/view/manager/report/ReportStockPackaging";
import ReportWithdrawPackaging from "@/view/manager/report/ReportWithdrawPackaging";
import ReportAddPackaging from "@/view/manager/report/ReportAddPackaging";
import ReportGetPackaging from "@/view/manager/report/ReportGetPackaging";

import ReportStockProduct from "@/view/manager/report/ReportStockProduct";
import ReportWithdrawProduct from "@/view/manager/report/ReportWithdrawProduct";

import ReportStockStaple from "@/view/manager/report/ReportStockStaple";
import ReportAddStaple from "@/view/manager/report/ReportAddStaple";
import ReportWithdrawStaple from "@/view/manager/report/ReportWithdrawStaple";



import ProductionManageWithdrawStaple from "@/view/production/managewithdrawstaple/ManageWithdrawStaple";
import ProductionAddWithdrawStaple from "@/view/production/managewithdrawstaple/AddWithdrawStaple";

import ProductionManageWithdrawProduct from "@/view/production/managewithdrawproduct/ManageWithdrawProduct";


import ProductionManageWithdrawPackaging from "@/view/production/managewithdrawpackaging/ManageWithdrawPackaging";
import ProductionAddWithdrawPackaging from "@/view/production/managewithdrawpackaging/AddWithdrawPackaging";
// import ProductionUpdateWithdrawPackaging from "@/view/production/managewithdrawpackaging/UpdateWithdrawPackaging";

import ProductionManageProducttion from "@/view/production/manageproducttion/ManageProducttion";
import ProductionAddNewProducttion from "@/view/production/manageproducttion/AddNewProducttion";

import TransportManageShipping from "@/view/transport/ManageShipping";
import CreateInvoice from "@/view/transport/CreateInvoice";
import TransportAddPackaging from "@/view/transport/AddPackaging";

Vue.use(Router);
export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      redirect: '/home',
    },    
    {
      path: '/home', 
      component: Home
    },
    {
      path: '/cart', 
      component: Cart
    },
    {
      path: '/signin', 
      component: Signin
    },
    {
      path: '/signup', 
      component: Signup
    },
    {
      path: '/orderconfirm', 
      component: OrderConfirm
    },
    {
      path: '/editprofile', 
      component: EditProfile
    },
    {
      path: '/edituserpassword', 
      component: EditUserPassword
    },
    {
      path: '/adminsignin', 
      component: SigninEmployee
    },
    {
      path: '/manage/product', 
      component: ManageProduct
    },
    {
      path: '/manage/addproduct', 
      component: AddProduct
    },
    {
      path: '/manage/updateproduct', 
      component: UpdateProduct
    },
    {
      path: '/manage/staple', 
      component: ManageStaple
    },
    {
      path: '/manage/addstaple', 
      component: AddStaple
    },
    {
      path: '/manage/updatestaple', 
      component: UpdateStaple
    },
    {
      path: '/manage/packaging', 
      component: ManagePackaging
    },
    {
      path: '/manage/addpackaging', 
      component: AddPackaging
    },
    {
      path: '/manage/updatepackaging', 
      component: UpdatePackaging
    },
    {
      path: '/manage/employee', 
      component: ManageEmployee
    },
    {
      path: '/manage/addemployee', 
      component: AddEmployee
    },
    {
      path: '/manage/updateemployee', 
      component: UpdateEmployee
    },
    {
      path: '/manage/order', 
      component: ManageOrder
    },
    {
      path: '/manage/addneworder', 
      component: AddNewOrder
    },
    {
      path: '/manage/withdrawstaple', 
      component: ManageWithdrawStaple
    },
  
    {
      path: '/manage/updatewithdrawstaple', 
      component: UpdateWithdrawStaple
    },
    {
      path: '/manage/withdrawpackaging', 
      component: ManageWithdrawPackaging
    },
   
    {
      path: '/manage/updatewithdrawpackaging', 
      component: UpdateWithdrawPackaging
    },
    {
      path: '/manage/withdrawproduct', 
      component: ManageWithdrawProduct
    },
 
    {
      path: '/manage/updateproducttion', 
      component: UpdateProducttion
    },
    
    {
      path: '/manage/shipping', 
      component: ManageShipping
    },
    {
      path: '/manage/addnewshipping', 
      component: AddNewShipping
    },
    {
      path: '/manage/producttion', 
      component: ManageProducttion
    },
    // {
    //   path: '/manage/addproducttion', 
    //   component: AddNewProducttion
    // },
    {
      path: '/manage/cost', 
      component: Cost
    },



    //report
    {
      path: '/manage/reportcost', 
      component: ReportCost
    },
    {
      path: '/manage/reportproducttion', 
      component: ReportProducttion
    },
    {
      path: '/manage/reportsale', 
      component: ReportSale
    },
  
    {
      path: '/manage/reportstockproduct', 
      component: ReportStockProduct
    },
    {
      path: '/manage/reportwithdrawproduct', 
      component: ReportWithdrawProduct
    },
    {
      path: '/manage/reportstockpackaging', 
      component: ReportStockPackaging
    },
    {
      path: '/manage/reportwithdrawpackaging', 
      component: ReportWithdrawPackaging
    },
    {
      path: '/manage/reportaddpackaging', 
      component: ReportAddPackaging
    },
    {
      path: '/manage/reportgetpackaging', 
      component: ReportGetPackaging
    },

    {
      path: '/manage/reportstockstaple', 
      component: ReportStockStaple
    },
    {
      path: '/manage/reportwithdrawstaple', 
      component: ReportWithdrawStaple
    },
    {
      path: '/manage/reportaddstaple', 
      component: ReportAddStaple
    },


    












    {
      path: '/producttion/withdrawstaple', 
      component: ProductionManageWithdrawStaple
    },
    {
      path: '/producttion/addwithdrawstaple', 
      component: ProductionAddWithdrawStaple
    },
    {
      path: '/producttion/withdrawproduct', 
      component: ProductionManageWithdrawProduct
    },
  
    {
      path: '/producttion/withdrawpackaging', 
      component: ProductionManageWithdrawPackaging
    },
    {
      path: '/producttion/addwithdrawpackaging', 
      component: ProductionAddWithdrawPackaging
    },
    // {
    //   path: '/producttion/updatewithdrawpackaging', 
    //   component: ProductionUpdateWithdrawPackaging
    // },
    {
      path: '/producttion/producttion', 
      component: ProductionManageProducttion
    },
    {
      path: '/producttion/addproducttion', 
      component: ProductionAddNewProducttion
    },
    {
      path: '/transport/shipping', 
      component: TransportManageShipping
    },
    {
      path: '/transport/createinvoice', 
      component: CreateInvoice
    },
    {
      path: '/transport/addpackaging', 
      component: TransportAddPackaging
    },
  ],
});