import Vue from 'vue'
import App from './App.vue'


import '@/css/font.css'
import '@/css/button.css'
import '@/css/sidebar.css'
import '@/css/table.css'
import '@/css/navbar.css'
import '@/css/form.css'

import store from './store'
import VueToast from 'vue-toast-notification';
import 'vue-toast-notification/dist/theme-default.css';
import  BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import router from './router'
import moment from 'moment'
import VueSidebarMenu from 'vue-sidebar-menu'
import 'vue-sidebar-menu/dist/vue-sidebar-menu.css'

import Vuelidate from 'vuelidate'

Vue.use(Vuelidate)

Vue.use(VueSidebarMenu)
Vue.use(BootstrapVue)
Vue.use(VueToast, {
  position: 'top'
})

Vue.$toast.clear();

Vue.filter("dateFomatNotTimeEn", function(date) {
  let res = '';
  if (typeof date === 'undefined' || date === null || date === '') {
    res = '-';
  } else {
    res =  moment(date).format('DD-MM-YYYY')
  }
  return res;
  console.log("res",res)
});

Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

Vue.config.productionTip = false
new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
