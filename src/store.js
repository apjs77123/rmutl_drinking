import Vue from 'vue';
import Vuex from 'vuex';

import  initState from './store/initState'
import  getters from './store/getters'
import  action from './store/actions'
import  mutations from './store/mutations'
import  createPersistedState from "vuex-persistedstate";

Vue.use(Vuex);
const plugins= [createPersistedState()];
export default new Vuex.Store({
    state:initState(),
    getters,
    action,
    mutations,
    plugins,
});