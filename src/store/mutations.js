import initState from './initState';
export default{
  setUser(state,payload){
    state.user=payload
  },
  addOrder(state,payload){
    state.order=payload
  },
  addCart(state,payload){
    state.cart=payload
  },
  sendId(state,payload){
    state.idorder=payload
  },
  calOrder(state,payload){
    state.ordersumary=payload
  },
  getIdProduct(state,payload){
    state.idproduct=payload
  },
  IdEmployee(state,payload){
    state.idemployee=payload
  },
  id(state,payload){
    state.id=payload
  },
  
  
  signOut(state){
    const initial = initState();
    Object.keys(initial).forEach((key) => {
      state[key] = initial[key];
    });
  }
}